# Minimal Fennel with Raylib 4.5
This project is a minimal setup required to run Raylib 4.5 using Fennel 1.3.0

## What's required
Dependencies required to run this example. You need to have a `main.lua` in the root directory, that is where the raylib executables look for it. Everything else can go into `./src/`.

### Windows
 - You need the `raylua_e.exe` and `raylua_s.exe` placed inside `./bin/`. 

- Place inside `./lib/`
  - `fennel.lua`

### Linux
Tested with EndevourOS (Archlinux)
 - Best to follow the instructions to clone and compile. Then you can use `raylua_e` and `raylua_s`

## How to use
 - Running
    - Windows use `./script/run.cmd`
    - Linux use `./script/run.sh`
 - Compile
    - Windows use `./script/compile.cmd`
    - Linux use `./script/compile.sh`

Compiling will create an executable with the current date_time_game(.exe) formate and store it in the `./build/` directory.

## Additionals
 - Tsnake41 Raylib Lua bindings https://github.com/TSnake41/raylib-lua
 - Fennel https://fennel-lang.org/