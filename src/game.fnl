(local screen-width 800)
(local screen-height 450)

(rl.InitWindow screen-width screen-height "Minimal Raylib")
(rl.SetTargetFPS 60)
(local window-icon (rl.LoadImage "./assets/img/fennel-logo.png"))
(rl.SetWindowIcon window-icon)

(var camera {})
(set camera.position {:x 0.0 :y 10.0 :z 10.0})
(set camera.target {:x 0.0 :y 0.0 :z 0.0})
(set camera.up {:x 0.0 :y 1.0 :z 0.0})
(set camera.fovy 45.0)
(set camera.projection rl.CAMERA_PERSPECTIVE)  


(local cube-position {:x 0.0 :y 0.0 :z 0.0})


(while (not (rl.WindowShouldClose))

  (rl.BeginDrawing)
  (rl.ClearBackground rl.WHITE)
  (rl.BeginMode3D camera)
  (rl.DrawCube cube-position 2.0 2.0 2.0 rl.RED)
  (rl.DrawCubeWires cube-position 2.0 2.0 2.0 rl.MAROON)
  (rl.DrawGrid 10 1.0)

  (rl.EndMode3D)
  (rl.DrawText "Welcome to the third dimension!" 10 40 20 rl.DARKGRAY)
  (rl.DrawFPS 10 10)
  (rl.EndDrawing))
(rl.CloseWindow)